<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('api/tasks', function(){

	return App\Task::all();
});

Route::get('/', function () {	

    return view('welcome');
});

Route::get('/component', function () {	

    return view('component');
});

Route::get('/custom-directives', function () {	

    return view('custom-directives');
});

Route::delete('/post/{post}', function (App\Post $post) {	

    $post->delete();

});