Vue.component('tasks', {

	template: '#task-template',	

	data: function(){

		return {
			list: []
		};		
	},

	created: function() {

		this.fetchTaskList();
	},

	methods: {

		fetchTaskList: function(){
			
			var resource = this.$resource('/api/tasks/{id}');

			resource.get(function(tasks){

				this.list = tasks;

			}.bind(this));
		}
	}

});

new Vue({

	el: 'body'

});