<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        </head>
    <body>
        <div class="container">
            <div id="app">
                                
                <alert type="success">
                    
                    <strong>Success!</strong> Your account has been updated.

                </alert>

                <alert type="warning">
                    
                    <strong>Warning!</strong> Please try again.

                </alert>


                <template id="alert-template">
            
                    <div :class="alertClasses" v-show="show">
                        
                        <slot></slot>

                        <span class="alert-close" @click="show = false">x</span>

                    </div>
                               

                </template>

            </div>
        </div>


        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.21/vue.js"></script>        
        <script type="text/javascript" src="/js/component.js"></script>
    </body>
</html>
