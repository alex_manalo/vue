<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        </head>
    <body>
        <div class="container">
            <div id="app">
                                
                <tasks></tasks>

            </div>
        </div>

        <template id="task-template">
            
            <h1>My Tasks</h1>

            <ul class="list-group">

                <li class="list-group-item" v-for="task in list" track-by="$index"> @{{ task.body }} </li>

            </ul>                

        </template>
        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.21/vue.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
        <script type="text/javascript" src="/js/main.js"></script>
    </body>
</html>
