<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        </head>
    <body>
        <div class="container">
            <div id="app">
                                
                <form method="POST" action='/post/6' v-ajax complete="Post has been deleted.">

                    {{ method_field('DELETE') }}

                    {{ csrf_field() }}
                    
                    <button type="submit">Delete Post</button>

                </form>

            </div>
        </div>


        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.21/vue.js"></script>  
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.min.js"></script>      
        <script src="/js/custom-directives.js"></script>
    </body>
</html>
